---
title: ""
icon: "fa-group"
weight: 1
---

<img src="logo.jpeg" class="center" width="300" height="300">

#### Um was gehts?
Der Chaostreff Ludwigsburg ist ein lockeres und offenes Zusammentreffen von Menschen, die sich dem CCC und der [Hackerethik](https://www.ccc.de/de/hackerethik) nahe fühlen.

Neugierige und Interessierte, die sich für Themen rund um Technik und Datenschutz interessieren, sind uns stets willkommen!

#### Wann und Wo trefft ihr euch?

Das nächste Treffen findet am 03.05.2022 ab 19:30 online unter https://meet.jit.si/chaostrefflb0001 statt.


Du erreichst uns außerdem per Mail über chaostreff AT complb PUNKt de


Im Rahmen des Chaostreffs gibt es auch immer wieder Kurzvorträge oder Ausflüge, mehr Infos dazu unter https://complb.de/vas

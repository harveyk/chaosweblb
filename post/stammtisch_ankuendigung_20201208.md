---
date: "2020-12-02"
tages: ["chaostreff", "veranstaltung"]
title: "Chaostreff Ankündigung 08.12.2020 - GPG Keysigning Party"
---
Hi!

Beim nächsten Chaostreff Ludwigsbu|r|g wollen wir eine kleine Keysigning-Party (remote) machen und uns GnuPG grundsätzlich anschauen: Was steht eigentlich in so einem öffentlichen Schlüssel drin? Wie sieht das Dateiformat aus? Wie funktionieren Subkeys? 

Das ganze nicht als Vortrag, sondern im lockeren Austausch. Natürlich geht es nicht nur um GnuPG, sondern um all die anderen Dinge, über die man so reden kann ;)


ampoff
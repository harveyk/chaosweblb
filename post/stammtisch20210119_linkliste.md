---
date: "2021-01-25"
tages: ["chaostreff", "linkliste"]
title: "Linksliste Chaostreff 19.01.2021"
---

Am 19.01.2021 fand der erste Chaostreff Ludwigsburg im zweiten Corona-Jahr per Jitsi statt. Über folgende Themen haben wir gesprochen und zu manchen haben wir Links im Chat gesammelt, die wir hier nochmal aufführen. 

Bleibt gesund und hackt Sachen!



- Warum der Einsatz von Windows 98 SE auch heute noch Sinn machen kann und was man dafür benötigt (eine lange Linkliste, die angefragt werden kann, hier aber nur vieeel Raum einnehmen würde), u.a [Creopard](https://www.creopard.de/)

- Die u.a. beim RC3 eingesetzte Plattform [Workadventu.re](https://workadventu.re/)


- Die Drohne [Tello](https://www.ryzerobotics.com/de/tello) 

- und dazu [Mods für die Tello aus dem 3d-Drucker](https://www.thingiverse.com/search?q=tello&type=things&sort=relevant&page=1)

- [3d-Druck in der Stabi Ludwigsburg](https://stabi.ludwigsburg.de/start/angebote/3D-Druck+und+3D-Scan.html)

- [OpenSCAD](https://www.openscad.org/)


- Einführung des Terms BKBH

- Die großartig gemachte [D.J. Trump Library](https://djtrumplibrary.com/)

- Das Spiel [Among Us](https://store.steampowered.com/app/945360/Among_Us/)

- Der Film [Die zwei Päbste](https://de.wikipedia.org/wiki/Die_zwei_P%C3%A4pste)

u.v.m.
